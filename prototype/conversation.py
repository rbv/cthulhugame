#!/usr/bin/env python3

from util import *
from basis import *

class Action:
    """Actions can appear as the head of a formula. They are procedures which have
    side effects, and are executed (with the rest of the form as arguments)when
    they appear on the RHS of a rule that is activated during forward inference.
    are simply wrappers around Python functions.

    After executing they return a pair (replacement_head, value),
    where value is the same as a value returned from a pure python function, while
    replacement_head is something to replace the Action with, so that its side effect
    doesn't have to happen again. Usually a Pred (e.g. the 'Tell' Action could return
    the 'Told' Pred) or a pure function.

    @Action can be used as a function decorator.
    """

    def __init__(self, func):
        self.func = func

    def __call__(self, *args):
        return self.func(*args)


class Rule:
    def __init__(self, conditions, effects, strength = 0):
        #assert isinstance(conditions, tuple)  # Either a Pred call or 
        if isinstance(conditions, tuple):
            conditions = [conditions]
        if isinstance(effects, tuple):
            effects = [effects]

        self.conditions = conditions
        self.effects = effects
        #self.twoway = False   # if and only if
        self.strength = strength

    def check(self, world, env):
        """Return whether the conditions hold.
        env is a dict containing Variable assignments"""

    def do_effects(self):
        pass

    def apply(self):
        if self.check():
            self.do_effects()
            return True
        return False


def isvariable(term):
    "Whether a term in a formula is a variable"
    return isinstance(term, str) and term.startswith('_')

# Syntactic sugar for variables
subj = '_subject'
w = '_w'
x = '_x'
y = '_y'
z = '_z'

#TODO: copy to Schemat
def unify(template_form, instance_form, env = {}):
    """Returns either False or a dict with variable assignments that unify the two forms.
    Only template_form may contain variables and '?' and  '*' wildcards!
    '*' wildcards must be at the end, and match zero or more terms. These are returned
        in the assignment dict, keyed by '*'.
    '?' wildcards are equivalent to anonymous variables, whose values are discarded.
    Any variables whose values are already fixed in env are also copied into the dict.
    """
    assignments = {}
    assert '*' not in instance_form
    assert '?' not in instance_form
    for idx in range(len(template_form)):
        tterm = template_form[idx]
        if tterm == '*':
            assert idx == len(template_form) - 1
            assignments['*'] = instance_form[idx:]
            return assignments
        if idx >= len(instance_form):
            return False

        iterm = instance_form[idx]
        assert not isvariable(iterm), "2nd form shouldn't contain variables"  # Don't need to implement that?
        if isvariable(tterm):
            if tterm in env:
                # Already has a fixed value (given)
                if env[tterm] != iterm:
                    return False
                assignments[tterm] = iterm
            elif tterm in assignments:
                # Already has a fixed value
                if assignments[tterm] != iterm:
                    return False
            else:
                assignments[tterm] = iterm
        elif tterm == iterm:
            pass  # Continue on a match of literals
        elif tterm == '?':
            pass
        else:
            assert not isinstance(iterm, (tuple, list)), "Unimplemented"
            assert not isinstance(tterm, (tuple, list)), "Unimplemented"
            return False
    return assignments

def reduce_form(form, env):
    "Substitute variables into a formula. Doesn't invoke functions or reduce Actions"
    if isvariable(form):
        if form not in env:
            raise ValueError("Value of %s is missing from env" % form)
        return env[form]
    if not isinstance(form, tuple):
        return form

    head = form[0]
    args = form[1:]
    if callable(head) or ispredicate(head):
        # Includes Pred instances and raw user (python) function

        reducedargs = []
        for arg in args:
            reduced = reduce_form(arg, env)
            reducedargs.append(reduced)

        return [head] + reducedargs
    else:
        raise ValueError("Formula %s has unknown head, should be a predicate name like Tell" % (form,))

def eval_form(form, env):
    """Evaluate a formula:
    Evaluates the args (except when shortcut evaluating) and then the function, returning its result
    (which would usaully be a Pred return value like fuzzy or bool; an Object; or a list of objects.
    Returns a pair (reduced_form, result)
    """

    if isvariable(form):
        if form not in env:
            raise ValueError("Value of %s is missing from env" % form)
        return env[form], env[form]
    if not isinstance(form, tuple):
        return form, form   # Can this happen?

    head = form[0]
    args = form[1:]

    if callable(head) or ispredicate(head):
        # Includes Pred instances and raw user (python) function

        reducedargs = []
        callargs = []
        for arg in args:
            reduced, evalled = eval_form(arg, env)
            reducedargs.append(reduced)
            callargs.append(evalled)
            # if isinstance(arg, tuple):
            #     callargs.append(eval_form(arg, env))
            # else:
            #     reducedargs.append(arg)
            #     callargs.append(arg)

        print("eval'ing %s(%s)" % (head, callargs))
        if ispredicate(head):
            return [head] + reducedargs, [head] + reducedargs
        elif isinstance(head, Action):
            # The Action replaces itself with a form without side-effects
            replacement_head, value = head(*callargs)
            return [replacement_head] + reducedargs, value
        else:
            return [head] + reducedargs, head(*callargs)

    else:
        raise ValueError("Formula %s has unknown head, should be a predicate name like Tell" % (form,))




def infer_forms(form, rules, env, direction = '->'):
    """Returns a list of formualae which could be inferred from 'form' a set of rules,
    such as [(Subject, x, '*'), '->', (Person, x), ...],
    and an environment `env` of already assigned variables.
    `form` can't contain variables, but rules can.
    `direction` is one of '->', '<-' or '<->' for the inference direction"""


    def apply_rule(lhs, rhs):
        unification = unify(lhs, form, env)
        if unification is not False:
            # Substitute variables into the consequent 'effects'
            print("rhs", rhs, "unification:", unification)
            ret.append(reduce_form(rhs, unification))

    ret = []
    for relation in rules:
        lhs, imp, rhs = relation
        assert imp in ('->', '<->')
        if imp == '<->':
            apply_rule(lhs, rhs)
            apply_rule(rhs, lhs)
        else:
            if direction == '->':
                apply_rule(lhs, rhs)
        print("unify", form, lhs, env)
    return ret


# eg (Subject, x, *) -> ('Person', x)
relevance_relations = [
    #[(Subject, x, '*'), '->', (Person, x)],
    [(Told, subj, x, '*'), '->', (Person, x)],
    # [(Description_Of, x, '*'), '->', (Person, x)],
    # [(Description_Of, x, '*'), '->', (Event, x)],
    [(Visited_Location, subj, x, '*'), '->', (Location, x)],
]


def check_relations(form, env):
    return infer_forms(form, relevance_relations, env)

def questionable_topics(player, npc):
    """Produce a list of things the player can ask an NPC about. These are composed of things:
    the player knows which:
    -there exists a conversation rule with it as a antecedent, or
    -the player knows (Pred1 X) NPC knows (Pred2 Y) where  """


    ret = []


class NPCPersonality:
    def __init__(self):
        # Shared attibutes
        self.Bad_Good = 0.
        self.Timid_Dominant = 0.
        self.Faithless_Honest = 0.

        self.Life_Outlook = 0
        self.Wealth = 0.5   # Between 0 for poverty to 1 for rich
        self.Greed = 0   # Between -1 and 1

        self.Player_alignment = 0.2  # Positive: friendly, negative: hostile

        self.told_all = False

    def react_offer_bribe(self):
        if self.Wealth < 0.5:
            pass


    def tell(player, npc):
        pass

    def intimidate(player, npc):
        pass

    def interrogate(player, npc):
        if npc.ai.told_all:
            print(npc, "is flustered. \"I don't know any more!\"")
            npc.ai.Player_alignment -= 0.2
            return

        if npc.ai.Player_alignment > 0.5:
            print(npc, "tells you what they know")
            #npc.tell_all_to(player)
            npc.ai.told_all = True

# class ReceptionistAI:
#     def (npc, villian):
#     if (Description_Of, villian) in npc.knows:
#         npc.tells((Rented_Room, villian, "Room 5"))


def talk(player, npc):
    while True:
        _, opt = pick_option("You...", ["Tell...", "Interrogate...", "Intimidate", "Show object...", "Bribe"], cancel="Leave")
        if opt == "Tell...":
            pass
        elif opt == "Interrogate...":
            interrogate(player, npc)
        elif opt == "Leave":
            print("You take your leave")
            return
        else:
            print("(Unimplemented)")


@Action
def Tell(speaker, target, *info):
    """This is an action that can be used in the consequent of a conversation Rule;
    makes the speaker tell the target something"""
    if (Told, speaker, target, info) not in speaker.knows:
        # Haven't already told the target this
        #if info not in speaker.knows:
        speaker.learn((Told, speaker, target, info))
        target.learn(info)
        print("%s tells %s %s" % (speaker, target, info_to_str(info, target)))
    else:
        print("%s already told %s that %s" % (speaker, target, info_to_str(info)))


def player_sayings(act):
    if unify(act, (Tell, subj, '_target', (Description_Of, (Person, y)))):   #unimplemented
        return "I'm looking for %s."
    return

print(player_sayings((Description_Of, (Person, x))))  #unimplemented

# def npc_sayings(act):
#     if unify(act, (Tell, (Description_Of, (Person, x)))):


if __name__ == '__main__':
    npc = Actor()

    vx = 42
    print("unify", unify((Visited_Location, subj, x, '*'), (Visited_Location, npc, 42), {}))
    print("eval", reduce_form(("Foo", x), unify((Visited_Location, subj, x, '*'), (Visited_Location, npc, 42), {})))
    print(check_relations((Visited_Location, subj, vx), {}))
    sys.exit()

    clr.init(autoreset = True)

    player = Actor()
    npc = Actor()
    npc.ai = NPCPersonality()
    npc.ai.told_all = False
    villian = Actor()
    #npc.behaviour = [Rule((Description_Of, villian), '->', Tell((Rented_Room, villian, "Room 5")))]

    #npc.behaviour = [Rule(Interrogate, Tell((Rented_Room, villian, "Room 5")))]


    talk(player, npc)
