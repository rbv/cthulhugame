"""
Definitions of specific Maps, Actors, Items, Cases, etc.
"""

from basis import *
import random


class ShadyWitness(Actor):
    def describe(self):
        "Description used when listing the NPCs in sight."
        return "A " + self.gender + " in an overcoat"

    def describe_fully(self):
        "Description used when you talk to an NPC (defaults to same as describe())"
        return "A " + self.gender + " wearing a shabby overcoat, who watches you with suspicion"


class Cultist(Actor):
    def describe(self):
        return "Suspicious " + self.gender


class DinerOwner(Actor):
    def __init__(self, diner: Map):
        super().__init__()
        self.diner = diner

    def describe(self):
        return "Diner owner"

    def describe_fully(self):
        if self.parent == self.diner:  # parent is the map
            ret = "The owner of this diner"
        else:
            ret = "A " + self.gender + ", who looks like a waiter"
        ret += ", dressed in an apron"
        return ret

    def talk(self, with_whom):
        # Override talk() to add some flavour text
        super().talk(with_whom)  # Call normal Actor.talk() function (tell everything)
        print("The owner makes small talk about rumours of smugglers at the docks.")


class DinerMap(Map):
    """This is a constructor for an ordinary diner map, which by default isn't generated as a crime scene.
    Currently, just generate an NPC, nothing else."""

    def __init__(self):
        super().__init__()
        self.owner = DinerOwner(self)
        self.name = self.owner.firstname + "'s Diner"
        self.owner.set_position(None, self)

    def describe(self):
        return "The seats and tables in this cosy diner have been worn smooth by decades of customers. The smell of hearty soup and smoke from the kitchen scent the air."


class Receptionist(Actor):
    "Receptionist at a hotel. Currently specific to the abduction case, but shouldn't be."

    def __init__(self):
        super().__init__()
        self.case = None  # Is set by a kludge

    def describe(self):
        return "Receptionist"

    def describe_fully(self):
        return "A very friendly receptionist willing to speak without a bribe"

    def talk(self, with_whom):
        """Hard-coded logic until there's a way to specify how an NPC reacts to information.
        with_whom is the player."""
        villian = self.case.villian
        if (Description_Of, villian) in with_whom.knows:
            print("You give a description of %s and learn their room number (END OF DEMO!)" % villian)
            with_whom.learn([Rented_Room, villian, "Room 5, %s" % self.case.hotel.name])
        else:
            print("The receptionist doesn't know how to help you")


class HotelMap(Map):
    def __init__(self):
        super().__init__()
        self.name = "Motel Notell"
        self.receptionist = Receptionist()
        self.receptionist.set_position(None, self)

    def describe(self):
        return "The reception area of this small motel is cramped, with no potplants or guestbook in sight."


##
# Items
##

def use_player_notebook(actor, notebook):
    print("You doodle in the notebook")

player_notebook = ItemGen(name="Notebook", description="Your leather-bound notebook", on_use = use_player_notebook)

lit_match = ItemGen(name="Lit match", description="A lit match which never goes out!")

def use_matchbook(item, actor):
    "Light a match"
    item.uses -= 1
    print("You light a match and pocket it.")
    actor.add_object(lit_match())  # Add to inventory

# The following demonstrate three different ways to define an item: by creating an ItemGen,
# by defining a generator function, or by using .extend() on an existing ItemGen.

matchbook = ItemGen(name="Matchbook", description="A matchbook containing {item.uses} matches", uses = 12, on_use = use_matchbook)

def branded_matchbook(business_map: Map):
    "Item generator for a matchbook branded with a business (with contact info)"
    name = business_map.name
    # Create a unique ItemGen and then call it
    generator = matchbook.extend(name=name + " matchbook",
                                 description="A matchbook with the logo and address of " + name,
                                 on_acquire = lambda self, actor: actor.learn([Location_Of_Interest, business_map]))
    return generator()



################################################################################
#                                    Cases

def RandomWitness(map):
    "Randomly create a suitable NPC as a witness, put them on a map"
    npc = ShadyWitness()
    npc.set_position(None, map)
    return npc


class AbductionSacrificeCase(Case):
    "Example of a case, which generates instances of the above Maps, Actors and Items"

    def __init__(self):
        self.victim = Actor()  # A new Actor with a random name
        self.victim.dead = True  # Currently has no effect
        self.villian = Cultist()

        # Scene 1: the diner
        self.diner = DinerMap()
        self.witness1 = RandomWitness(self.diner)
        # Give the witness some pieces of information (which currently they always share when you talk with them)
        self.witness1.learn([Saw, None, "%s and %s at %s, they left together" % (self.victim, self.villian, self.diner)])
        self.witness1.learn([Description_Of, self.villian])

        # Scene 2: the hotel (reception area)
        self.hotel = HotelMap()
        self.hotel.receptionist.case = self  # To be cleaned up
        # Example of a physical clue
        hotel_matchbook = branded_matchbook(self.hotel)
        hotel_matchbook.set_position(None, self.diner)  # Put on the diner map

        self.initial_map = self.diner  # Where the player starts
        self.initial_fact = [Visited_Location, self.victim, self.diner]# Given to the player

        # For now, print a description of the case when you start it
        print(f"You've been hired by a distressed family to investigate the disappearance of {self.victim} two days ago. They were last seen at {self.diner}.")
