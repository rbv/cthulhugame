
"""Utility functions"""

import sys

try:
    import colorama as clr
    from colorama import Fore, Back, Style
except ImportError:
    print("Please install the colorama module:")
    print("python3 -m pip install colorama")
    sys.exit()


def pick_option(prompt_text, options, cancel = "Cancel"):
    """Prompt the player to pick from a menu of options, or cancel unless cancel=None.
    Returns a pair (option_num, option), where option_num is 0-indexed (while
    the player is given a 1-indexed menu), or (-1, cancel)"""
    if cancel is not None:
        options = [cancel] + options
        first = 0
    else:
        first = 1

    if prompt_text:
        print(Style.BRIGHT + prompt_text)
    for idx, option in enumerate(options):
        print("%2d. %s" % (first + idx, option))

    while True:
        try:
            selection = int(input(Fore.GREEN + "? "))
            if selection >= first and selection <= len(options):
                print(Fore.RESET) #, end="")
                return selection - 1, options[selection - first]
        except EOFError:  # Ctrl-D
            print("")  # Missing newline
            if cancel is not None:
                return -1, cancel
            sys.exit()
        except KeyboardInterrupt:  # Ctrl-C
            sys.exit()
        except:
            pass
        print("Try again.")


def debug_print(text):
    print(Style.DIM + Fore.BLUE + "[[" + text + "]]")

def clear_screen():
    print("\033[2J")

def move_cursor(x,y):
    return("\033[%d;%dH" % (x, y))
