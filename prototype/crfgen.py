from collections import defaultdict


class CRFModel:
    # Variable types
    TYPE_ENUM = 0
    TYPE_REAL = 1
    TYPE_COUNT = 2   # 0 or greater

    class EnumDistribution:
        def __init__(self, option_weights):
            """option_weights is a list of possible values, each optionally followed by a weight
            (defaults to 0, unless all are omitted; then they are equiprobable.).
            The weights don't have to sum to 1."""
            self.options = []
            self.weights = []
            total_weight = 0.0
            for idx in range(len(option_weights)):
                option = option_weights[idx]
                self.options.append(option)
                weight = 0.0  # Default
                if idx + 1 < len(option_weights):
                    it2 = option_weights[idx + 1]
                    if isinstance(it2, (float, int)):
                        weight = float(it2)
                        idx += 1
                total_weight += weight
                self.weights.append(weight)
            if total_weight:
                self.weights = [w/total_weight for w in self.weights]
            else:
                self.weights = [1.0/len(self.weights) for w in self.weights]

    

    def __init__(self):
        # Map from variable names to (type, va
        self.vars = {}
        self.graph = {}
    def enum_var(self, name, options):
        self.vars[name] = EnumVar(options)
    def relate(self, *args):
        pass
    def sample(self):
        indegree = defaultdict(int)
        for name, var in self.graph.iteritems:
            if 
        roots = [var for (var, in self.graph if 

dist = CRFModel()
# Separate 'none' from 'aristocrat' because the implications are quite different
dist.enum_var('profession', ['none', 0.1, 'labourer', 1, 'dock worker', 0.15, 'office', 0.3, 'businessman', 0.2, 'aristocrat', 0.01, 'professional', 0.1, 'doctor', 0.01, 'academic', 0.02])
# The following income distributes are defaults if not overridden
dist.enum_var('income', ['poverty', 0, 'low', 0.5, 'medium', 0.4, 'high', 0, 'landed', 0])
# If profession is none or aristocrat, use a different distribution for income:
dist.relate('profession=none -> income:', ['poverty', 0.3, 'low', 0.7])
dist.relate('profession=aristocrat -> income:', ['medium', 0.1, 'high', 0.2, 'landed': 0.7])
# If profession is any of these...
dist.relate('profession=doctor|businessman|professional -> income:', ['medium': 0.1, 'high': 0.8, 'landed': 0.1]])
# If profession is academic, change the chance (weight) of high income while leaving other weights the same
dist.relate('profession=academic -> income=high: 0.2')
