#!/usr/bin/env python3

import random
from util import *


################################################################################


# Actor: a single character (note: in Portlligat, called an Entity)
# Organisation: a cult or other group, such as a gang or the police force, or even the whole public or a segment of it.
# Entity: either an Actor or an Organisation

class MapObject:
    "Corresponds to Portlligat's MapObject. A thing that can be positioned on a map (or not, e.g. in inventory) with a sprite."
    obj_counter = 0

    def __init__(self, name = "<MapObject>"):
        self.name = name
        # position and parent are None if not on a Map
        self.parent = None
        self.position = None
        MapObject.obj_counter += 1
        self.obj_id = MapObject.obj_counter

    def set_position(self, position, map = None):
        if map:
            map.add_object(self)  # Sets .parent, wipes .position
        self.position = position
        # Update map.objects_at_tile
        #self.parent._set_obj_position(self, where)

    def unset_position(self):
        "Called to remove an object from its current tile"
        # if self.parent:
        #     # Update map.objects_at_tile
        #     self.parent._unset_obj_position(self)
        if self.parent:
            self.parent.remove_object(self)  # Wipes .parent, .position


class AbstractContainer:
    """Something that has a list or set of contents, which it owns, and recursively is in another AbstractContainer.
    The required type of the objects is not specified here.
    Examples: bag, actor inventory, actor equip, table top, map.
    The actual Container class possibly keeps additional extrinsic metadata on each, such as concealment.

    It's suggested that each contained object has members:
    -parent: the AbstractContainer that owns the object
    -position: optional parent-specific location data. The form of the data changes depending on parent
               (e.g. if an item is picked up on the map, .position changes from tile coord to inventory slot)
    """

    # parent is another AbstractContainer, unless it is a top-level Map.
    parent = None

    def add_object(self, obj):
        "Generic implemention of adding an object to the container by updating"
        if obj.parent is not self:
            if obj.parent:
                obj.parent.remove_object(obj)
            obj.parent = self
            obj.position = None
            return True

    def remove_object(self, obj):
        obj.parent = None
        obj.position = None


class Map(AbstractContainer):
    def __init__(self):
        self.parent = None  # In future we might have submaps
        self.objects = set()  # List of MapObjects, including Actors
        self.name = "<Map>"
        global world
        world.add_map(self)

    def add_object(self, obj: MapObject):
        """Move an MapObject (e.g. Actor, MapItem) to this map if not already there.
        Suggest calling obj.set_position() instead."""
        if super().add_object(obj):
            self.objects.add(obj)

    def remove_object(self, obj: MapObject):
        """Remove an MapObject (e.g. Actor, MapItem) from this map.
        Suggest calling obj.unset_position() or .set_position() instead."""
        assert obj.parent == self
        self.objects.remove(obj)  # Requires membership. Use .discard() instead?
        obj.parent = None
        obj.position = None

    def actors_who_can_see(self, what: MapObject):
        """List of all Actors that can see something on the map (currently, all of them);
        dual to .actors_in_view()."""
        return [obj for obj in self.objects if isinstance(obj, Actor) and obj != what]

    def objects_in_view(self, of_whom):
        """List of Actors that someone can see (currently, all of them)
        (Differs from Portlligat's MapManager.objects_in_view by not assuming Player)"""
        return list(self.objects)

    def actors_in_view(self, of_whom):
        "List of Actors that someone can see (currently, all of them)"
        return [obj for obj in self.objects_in_view(of_whom) if isinstance(obj, Actor)]

    def describe(self):
        return ""

    # def describe_fully(self):
    #     return self.describe()

    def __str__(self):
        return self.name


class Item(MapObject):
    """
    Note: in Portlligat, MapItem inherits from MapObject and owns an Item. We do things differently
    here, with one class instead of two, because here a MapObject doesn't have to actually be on a map.
    """

    name = None
    description = None

    # The parent is the container (e.g. bag, actor inventory, actor equip, table top, map) that owns this object,
    # which will itself probably have a parent.
    # The location in/on the parent, if any (e.g. a map coordinate, location on the table top, equip slot)
    # is stored in self.position.
    # The parent might also store additional data on the item such as its concealment state and ownership.
    parent = None  # an AbstractContainer

    # If not None, has limited uses
    uses = None

    def __init__(self, name, description, **kwargs):
        super().__init__(name)
        self.description = description
        self.__dict__.update(kwargs)

    # @staticmethod  # 'self' needs to be passed explicitly when calling this
    # def on_use(self, actor):
    #     "Hook called when actor attempts to use"
    #     pass

    # @staticmethod  # 'self' needs to be passed explicitly when calling this
    # def on_acquire(self, actor):
    #     "Hook called when an actor acquires this item"
    #     pass

    def __str__(self):
        return self.name # self.description 

# class MapItem(MapObject):
#     "Corresponds to Portlligat's MapItem. A wrapper around an Item that can be positioned/displayed on a map with a sprite."
#     def __init__(self, item: Item):
#         super().__init__()
#         self.item = item
#         self.noun = item


class ItemGen:
    """You don't have to use this class to define an item type; creating a function that returns a new Item would
    likely be more direct for an item with randomised attributes."""
    def __init__(self, **kwargs):
        self.kwargs = kwargs

    def __call__(self):
        return Item(**self.kwargs)

    def extend(self, **kwargs):
        "Returns a new ItemGen which is a modified (extended) copy of this one"
        extended = self.kwargs.copy()
        extended.update(kwargs)
        return ItemGen(**extended)


########################################

# Items of information ('info', aka formulas, 'form', or clauses) stored in
# Actor.knows are tuples (autoconverted from lists) rather than instances of the
# Information class, to match Schemat.

# The following is a list of predicates, which combine with arguments to form clauses (formulas).

# >> The following predicates always have an Actor as the subject (first arg)

# The value/description begins with "(v)" if it's a verb (forms a verb clause)
# describing something an Actor did.
# Verbs are things that the (implicit) subject of the verb did, so when transferred to
# another Actor they must be wrapped in a Subject predicate.
Visited_Location = "(v)visited location"             # <subj> Visited (knows) <map>
Saw = "(v)saw"                                       # <subj> Saw <description_str>
Rented_Room = "(v)rented room"                       # <subj> Rented <room description str>
Told = "(v)told"                                     # <subj> Told <actor> <information>

# >> The following predicates might not have Actor as the subject (first arg)

Location_Of_Interest = "is location of interest"        # Knows about and can travel to <map>

Description_Of = "description"                       # Have description of <something> usable for identification

Event = "is Event"                                   # Includes crimes
Person = "is Person"                                 # 
Location = "is Location"

Knows = "knows"                                      # <actor> knows <fact>.
                                                     # Note we assume actors know everything about themselves (Mx -> KxMx)


def ispredicate(pred):
    "Predicates are title-cased variables like 'Location_Of_Interest'"
    return isinstance(pred, str) and len(pred) > 1 #and pred.istitle()

def head_is_actor_verb(pred):
    "The head (pred) of a fact is a (v) actor verb"
    return ispredicate(pred) and pred.startswith("(v)")

def lists_to_tuples(fact):
    "Recursively convert lists to tuples so it's hashable"
    if isinstance(fact, (list, tuple)):
        return tuple(lists_to_tuples(x) for x in fact)
    return fact

def info_to_str(info, implicit_subject = None, implicit_verb = "knows", omit_subject = False):
    """Format an item of information as a string.
    implicit_subject is the default subject, if the subject of `info` is equal to this
    then it will be omitted. If implicit_subject is None then it w"""
    parts = ()
    verb, subject = info[:2]
    #if head_is_actor_verb(verb):
    if isinstance(verb, str):
        # The predicate is a verb... trim the (v)
        verb = verb.replace("(v)", "")
    if implicit_subject and subject != implicit_subject:
        if implicit_verb and omit_subject == False:
            parts = (implicit_subject, implicit_verb)  # Add prefix
        omit_subject = False
    if omit_subject and subject == implicit_subject: #omit_subject:
        # Using "I" here is a kludge that should be handled by SentenceGen
        subject = "I"
    # Change from VSO order to SVO
    parts = parts + (subject, verb) + info[2:]
    return " ".join(str(x) for x in parts)

def subject_knows_info_clause(subject, info):
    """Wrap an info in a Knows clause if suitable. (Not yet used)."""
    if info[1] == subject:
        # Assume that Actors know facts about themselves, so don't need to
        # create "X knows X ..." clauses
        return info
    # if info[0] == Knows:
    #     # Forbid "X knows Y knows ..."
    #     return [Knows, subject, info[2]]
    return [Knows, subject, info]


########################################
# Information/Event classes currently unused

class Information:
    """This class represents a piece of information which can be known by an Actor, and which can be transmitted
    in conversation. It can be used as a Clue. A composed of:
    -a predicate or "event", possibly compounded, such as "has item X" or "told X that Y"
    -optionally, a timestamp, for things that aren't unchanging facts

    The knower of the information is implicit:  whichever Actors has it in their .knows set.
    """
    when = None

class VisitEvent(Information):
    "Someone (who) has been somewhere (where), optionally 'when'."
    def __init__(self, who, where, when = None):
        self.who = who
        self.where = where
        self.when = when

class AcquireEvent(Information):
    "Someone (who) obtained some item (what), optionally 'when' and 'where'."
    def __init__(self, who, what, when = None, where = None):
        self.who = who
        self.what = what
        self.where = where
        self.when = when


########################################

class StateChange:
    """This is an atomic event considered by a CaseGenerator, the content of a single node of its graph. It can be:
    -an Entity learns Information
    -an Entity gains an Item
    """
    pass

class Case:
    def __init__(self):
        self.graph = []
        self.initial_map = None
        self.initial_fact = None


########################################


class Actor(MapObject, AbstractContainer):
    """A single character.
    Inherits from MapObject:
    -map and position members
    -set_position(position [, map])."""
    def __init__(self, name = None):
        if name == None:
            self.gender, name = Actor.random_gender_and_name()
        else:
            self.gender = "man"  # or "woman"
        super().__init__(name)
        self.firstname = name.split()[0]
        self.knows = set()
        self.inventory = []

    def __str__(self):
        return self.describe() + " (Actor %s)" % (self.obj_id,)

    @staticmethod
    def random_gender_and_name():
        "Used to generate random NPCs"
        # Gender & first name
        if random.randint(0, 1):
            gender = "man"
            name = random.choice(["Sam", "Neil", "Tom", "Hank", "Bruce", "Lee"])
        else:
            gender = "woman"
            name = random.choice(["Sam", "Alex", "Julia", "Jane", "Jill", "Loren"])
        # Surname
        name += " " + random.choice(["Neil", "Jones", "Robberts", "Smith", "de Jungle", "de Grey"])
        return gender, name

    def print_knowledge(self):
        print("%s knows:" % self)
        for info in self.knows:  # TODO, sort this somehow (items are incomparable)
            print("  " + info_to_str(info, self, omit_subject = True))

    def describe(self):
        "Default, typically overridden"
        return self.name

    # def __repr__(self):
    #     return "<Actor %s>" % self.npc_id

    def add_object(self, item):
        """Add an item to inventory, return True if wasn't already there.
        (If item is callable (eg. ItemGen), calls it to create a new Item)."""
        if callable(item):
            item = item()
        if super().add_object(item):
            self.inventory.append(item)
            if hasattr(item, "on_acquire"):
                item.on_acquire(item, self)
            return True

    def remove_object(self, item: Item):
        self.inventory.remove(item)

    def witnessable_fact(self, event_info):
        "Learn a fact (probably an action), and everyone who can see you also learns it."
        self.learn(event_info)
        if self.parent:
            for actor in self.parent.actors_who_can_see(self):
                actor.learn(event_info)

    def learn(self, info):
        "Learn a fact. As a shortcut, the subject defaults to self if None"
        if info[1] == None:
            info[1] = self
        # Make hashable
        info = lists_to_tuples(info)
        if info not in self.knows:
            self.knows.add(info)
            debug_print(info_to_str(info, self, "learnt"))
            #debug_print(info_to_str(info, None, "learnt"))

    def set_position(self, position, map = None):
        super().set_position(position, map)
        if self.parent:
            # You and everyone who can see you learns you were here
            # (this will only be new information when you first come into view)
            self.witnessable_fact([Visited_Location, self, map])
            # You also learn who else is already here
            for actor in self.parent.actors_in_view(self):
                self.learn([Visited_Location, actor, map])

            #self.learn(VisitEvent(self, map))

    def check_visited(self, map):
        #when = None
        for fact in self.knows:
            if fact == (Visited_Location, self, map):
                return True
                #when = max(when, fact.when)
        return False
        #return when

    def talk(self, with_whom):
        "Placeholder result: each tells the other everything."
        print("%s talks to %s." % (self, with_whom))
        
        def transfer_knowledge(teacher, student):
            for info in teacher.knows:
                if info not in student.knows:
                    student.learn(info)
                    #print(info_to_str(info, student, "learnt"))
        
        transfer_knowledge(with_whom, self)
        transfer_knowledge(self, with_whom)


class World:  # An AbstractContainer?
    def __init__(self):
        self.maps = set()
        self.cases = []

    def add_map(self, map):
        "Register a newly created map. Called automatically by every Map constructor."
        self.maps.add(map)


world = World()
