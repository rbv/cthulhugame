#!/usr/bin/env python3

"""
Contains the Player class, which has all the user interface code.
This is the module to execute: it instantiates the world and puts the player in it.
"""

import sys

from basis import *
from util import *
import content


################################################################################


class Player(Actor):
    def __init__(self):
        super().__init__()
        self.name = "You"

    def describe(self):
        return "Player"

    def known_locations(self):
        "Locations you can travel to"
        return [info[1] for info in self.knows if info[0] == Location_Of_Interest]

    def enter_map(self, where):
        print("You travel to", where)
        self.set_position(None, where)
        print("You are at %s. %s" % (where.name, where.describe()))
        print("You see:")
        for obj in where.objects:
            if obj is not self:
                print(" ", Fore.LIGHTCYAN_EX + str(obj))

    def travel_menu(self):
        _, where = pick_option("You know (can travel) to the following locations:", self.known_locations())
        if where != "Cancel":
            self.enter_map(where)

    def talk_menu(self):
        actors = self.parent.actors_in_view(self)
        if self in actors:
            actors.remove(self)
        if len(actors) == 0:
            print("There's noone here.")
            return
        _, whom = pick_option("Whom do you talk to?", actors)
        if whom != "Cancel":
            print(whom.describe_fully())
            whom.talk(self)   # So that 'whom' can override

    def pickup_menu(self):
        items = [obj for obj in self.parent.objects if isinstance(obj, Item)]
        if len(items) == 0:
            print("There's nothing here you can take with you.")
            return
        idx, item = pick_option("What do you pick up?", items)
        if idx >= 0:
            self.add_object(item)
            print(item.description.format(item = item))

    def use_menu(self):
        if len(self.inventory) == 0:
            print("You don't have anything.")
            return
        idx, item = pick_option("What do you use?", self.inventory)
        if idx >= 0:
            #print(item.describe_fully())
            print(item.description)
            #if hasattr(item, "uses") and item.uses <= 0:
            if item.uses is not None and item.uses <= 0:
                print("The %s is spent" % (item,))
            elif hasattr(item, "on_use"):
                item.on_use(item, self)
            else:
                print("You don't know what to do with this.")

    def act(self):
        "The main loop of the prototype game"
        while True:
            print()
            _, opt = pick_option("What do you do?",
                                 ["Travel elsewhere", "Talk to person", "Use object from inventory", "Take object", "Examine notes"], cancel="Quit")
            if opt == "Quit":
                print("You walk away, and evil overruns the world.")
                sys.exit()
            elif opt == "Travel elsewhere":
                self.travel_menu()
            elif opt == "Talk to person":
                self.talk_menu()
            elif opt == "Use object from inventory":
                self.use_menu()
            elif opt == "Take object":
                self.pickup_menu()
            elif opt == "Examine notes":
                self.print_knowledge()


if __name__ == '__main__':

    # Top-level

    clr.init(autoreset = True)
    clear_screen()
    print(move_cursor(1,1) + Style.BRIGHT + "\n  <<< CTHULHUGAME PROTOTYPE >>>")
    print(Style.DIM + "                  (it's real!)\n")

    world.player = Player()
    case = content.AbductionSacrificeCase()
    world.cases.append(case)
    world.player.add_object(content.player_notebook())
    world.player.learn([Location_Of_Interest, case.initial_map])
    world.player.learn(case.initial_fact)
    world.player.enter_map(case.initial_map)
    world.player.act()
